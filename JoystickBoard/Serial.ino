//
// file: Serial.ino
//

void initSerial()
{
  Serial.begin(9600);
}

// Rx buffer
boolean inBufferFree()
{
  boolean free=true;

  free=((inSerialRead!=0) ? ( ((inSerialWrite+1)==inSerialRead) ? false : true ):
  ( (inSerialWrite+1==inSerialSize) ? false : true ));

  return free;
}

// Receive data into Rx buffer
void serialEvent() 
{
  while ((Serial.available()) && inBufferFree()) 
  {    
    // get the new byte:
    char inChar=(char)Serial.read();

    // discard non printables except for newline
    if ((inChar>31) || (inChar=='\n'))
    {
      // if the incoming character is a newline, set a flag
      // so the main loop can do something about it:
      if (inChar==';') {
        inSerialComplete=true;
      }
      // add it to the inputString:
      inSerialBuffer[inSerialWrite++]= inChar;
    }

    if (inSerialWrite>=inSerialSize)
    {
      inSerialWrite=0;
    }
  }

}

void processResponses(void)
{
  bool ignoreNewLine=true;
  int mySerialRead=inSerialRead;
  int responseBegin=inSerialRead;
  int lastNewLine=inSerialRead;

  while (mySerialRead!=inSerialWrite)
  {
    char k=inSerialBuffer[mySerialRead];

    if ((ignoreNewLine) && (k!=';'))
    {
      ignoreNewLine=false;
      responseBegin=mySerialRead;
    }

    if ((k==';') && (!ignoreNewLine))
    {
      processResponse(responseBegin,mySerialRead);
      lastNewLine=mySerialRead;
      ignoreNewLine=true;
    }

    mySerialRead++;

    if (mySerialRead>=inSerialSize)
    {
      mySerialRead=0;
    }
  }

  inSerialRead=lastNewLine;
}

char getCharAt(int rBegin, int offset)
{
  int ndx;

  if ((rBegin+offset)>(inSerialSize-1))
  {
    ndx=(rBegin+offset)-inSerialSize;
  }
  else
  {
    ndx=rBegin+offset;
  }

  return inSerialBuffer[ndx];
}

void processResponse(int rBegin, int rEnd)
{
  byte heli=getCharAt(rBegin,0);
  char cmd=getCharAt(rBegin,1);
  byte paramstart=2;
  byte param=0;
  byte ndx=0;
  bool percent=false; 
  bool terminator=false;
  bool parampresent=false;
  bool paramerror=false;

  // if heli # is too big, may be a command A; ?; S;
  // clear paramerror
  if (((heli=='A') || (heli=='D') || (heli=='J') || (heli=='M') || (heli=='V') || (heli=='H') || (heli=='X') || (heli=='?')))
  {
    cmd=heli;
    heli='0';

    terminator=true;
    paramerror=false;
    paramstart=1;
  }
  heli-='0';

  // extract parameter first...
  for (int i=0; i<4; i++)
  {
    char c = getCharAt(rBegin,paramstart+i);

    if (c=='%')
    {
      percent=true;
      ndx++;
      break;
    }

    if (c==';')
    {
      terminator=true;
      break;
    }

    if ((c>='0') && (c<='9'))
    {
      parampresent=true;
      param=param*10+(c-'0');
      ndx++;
    }
    else
    {
      paramerror=true;
    }
  }

  if ((!terminator) && (getCharAt(rBegin,2+ndx)==';'))
  {
    terminator=true;
  }

  if ((percent) && (param>100))
  {
    paramerror=true;
  }

  if ((heli>=0) && (heli<=4) && terminator && !paramerror)
  { 
    switch(cmd)
    {
    case 'N':
      initHeli(&Helis[heli],param);
      break;

    case 'C':
      setCH(&Helis[heli],param);     
      break;

    case 'B':
      bindHeli(&Helis[heli]);
      break;

    case 'G':
      if (percent)
      {
        setThrottlePercent(&Helis[heli],param);
      }
      else
      {
        setThrottleBits(&Helis[heli],param);        
      }
      break;

    case 'R':
      if (percent)
      {
        setRudderPercent(&Helis[heli],param);
      }
      else
      {
        setRudderBits(&Helis[heli],param);        
      }    
      break;

    case 'P':
      if (percent)
      {
        setPitchPercent(&Helis[heli],param);
      }
      else
      {
        setPitchBits(&Helis[heli],param);        
      }    
      break;   

    case 'T':
      if (percent)
      {
        setTrimPercent(&Helis[heli],param);
      }
      else
      {
        setTrimBits(&Helis[heli],param);        
      }    
      break;

    case 'S':
      showHeliState(&Helis[heli]);
      break;

    case 'A':
      for (int i=0; i<4; i++)
      {
        Serial.print(F(" I"));
        Serial.print(i);
        Serial.print(F(": "));
        Serial.print(AD[i]);
      }
      Serial.println(F(""));   

      Serial.print(F(" Button A:"));
      Serial.print(digitalRead(6));
      Serial.print(F(" Button B:"));
      Serial.print(digitalRead(7));
      Serial.println(F(""));   

      break;

    case 'D':
      setup_IRIn();
      while (1)
      {
        loop_IRIn();
      }
      break;

    case 'J':
      if (param==1)
      {
        initController();
        eepromValues.joystickEnabled=true;
        eeprom_write_block((const void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));         
      }

      if (param==0)
      {
        stopController();
        eepromValues.joystickEnabled=false;
        eeprom_write_block((const void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));         
      }

      break;

    case 'H':
      if (eepromValues.joystickEnabled)
      {
        Serial.println(F("Joystick active, changes will take effect next time board reboots or joystick enabled"));
      }

      if ((param>0) && (param<4))
      {
        eepromValues.heliType=param;
        eeprom_write_block((const void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));
      }
      else
      {
        Serial.println(F("helicopter parameter not 1,2,3"));
      }
      break;

    case 'X':

      if ((param>0) && (param<3))
      {
        eepromValues.shieldType=param;
        eeprom_write_block((const void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));
      }
      else
      {
        Serial.println(F("board type parameter not 1,2"));
      }        
      break;

    case 'M':
      //Serial.println(F(" M command empty"));
      //paramerror=true;
      break;

    case 'V':

      MS5_setup();
      while (1)
      {
        MS5_loop();
      }
      break;

    case '?':
      Serial.println(F("command sintax:"));
      Serial.println(F("<heli><command><value>[<%>];"));
      Serial.println(F("heli: heli number 0..9"));
      Serial.println(F("commands, shown in order needed to fly:"));
      Serial.println(F("N - initialize new heli 1-PROTOCOL TURBO HAWK 2-DAVE & BUSTER 2CH 3-UFO - 1N1;"));
      Serial.println(F("C - set channel 0,1,2 - 1C1;"));
      Serial.println(F("B - bind heli - 1B;"));
      Serial.println(F("G - set throttle, value is a percentage if last character is %- 1G10%;"));
      Serial.println(F("R - set rudder, value can be percentage or heli specific - 1R8;"));
      Serial.println(F("P - set pitch, value can be percentage or heli specific - 1P8;"));
      Serial.println(F("T - set trim, value can be percentage or heli specific - 1T20;"));
      Serial.println(F("S - shows heli variables - 1S;"));
      Serial.println(F("A - shows ADC values - A;"));
      Serial.println(F("D - decode infrared procotols - D; EXIT WITH RESET"));
      Serial.println(F("J - disable or enable joystick control of heli 0 - J0; or J1;"));
      //Serial.println(F("M - map joystick to controls of heli 0"));
      Serial.println(F("H - set joystick heli type 1,2,3 - H1;"));
      Serial.println(F("X - set IR Shield version 1,2 - X2;"));
      Serial.println(F("V - view EEPROM contents - V; EXIT WITH RESET"));      
      Serial.println(F("? - prints this help - ?;"));  
      Serial.println(F("example: create a turbohawk heli on slot 1, set it to channel 1, bind it & start it at 15% throttle"));  
      Serial.println(F("1N1;1C1;1B;1G15;"));
      Serial.println(F("2N2;2C1;2B;2G15;"));
      Serial.println(F("3N3;3C1;3B;3G15;"));      
      break;    

    default:
      Serial.print(F("command error"));
      paramerror=true;
      break;

    }

  }
  else
  {
    Serial.print(F("parameter error"));
    paramerror=true;
  }

  if (paramerror)
  {
    Serial.print(F(" begin:"));
    Serial.print(rBegin);
    Serial.print(F(" end:"));
    Serial.print(rEnd);
    Serial.print(F(" heli:"));
    Serial.print(heli);
    Serial.print(F(" cmd:"));
    Serial.print(cmd);
    Serial.print(F(" param:"));
    Serial.print(param);
    Serial.print(F(" percent:"));
    Serial.print((percent)?F("true"):F("false"));
    Serial.print(F(" term:"));
    Serial.print((terminator)?F("true"):F("false"));
    Serial.println();    
  }

}
