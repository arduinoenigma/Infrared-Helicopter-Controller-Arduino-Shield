//
// file: JoystickBoard.ino
//

#include <MemoryFree.h>
#include <EEPROM.h>
#include <avr/eeprom.h>

#define EEPROMADD 66

#define IR_ON LOW
#define IR_OFF HIGH

#define IR_PIN 12
#define STATUS_PIN 13

// this can be a 9 or 10
#define DELAY_38kH 9

#define HELIINIT 109

#define CHA 0
#define CHB 1
#define CHC 2

//measured values
#define TURBOHAWK 1
#define TURBOHAWK_HEADER 1500
#define TURBOHAWK_ONE 800
#define TURBOHAWK_ZERO 400
#define TURBOHAWK_OFF 330
#define TURBOHAWK_PACKET 34*2+1
#define TURBOHAWK_CHA 0
#define TURBOHAWK_CHB 2
#define TURBOHAWK_CHC 3
#define TURBOHAWK_REPEAT 200000

#define TWOCH 2
#define TWOCH_HEADER 3620
#define TWOCH_ONE 2660
#define TWOCH_ZERO 930
#define TWOCH_OFF 1000
#define TWOCH_PACKET 13*2+1
#define TWOCH_CHA 0
#define TWOCH_CHB 2
#define TWOCH_CHC 1
#define TWOCH_REPEAT 200000
//#define TWOCH_REPEAT 48000

//measured values
#define UFO 3
#define UFO_HEADER 3100
#define UFO_ONE 2280
#define UFO_ZERO 970
#define UFO_OFF 1060
#define UFO_PACKET 14*2+1
#define UFO_CHA 1
#define UFO_CHB 2
#define UFO_CHC 0
#define UFO_REPEAT 200000
//#define UFO_REPEAT 50000

//156 bytes
typedef struct heliData_t
{
  byte Init;
  boolean Bound;
  byte BindPacketCount;
  byte HeliType;
  byte Throttle;
  byte Rudder;
  byte Pitch;
  byte CH;
  byte Trim;
  byte A;
  byte B;
  byte Check;

  int Header;
  int One;
  int Zero;
  int Off;

  unsigned long RepeatRate;
  unsigned long RepeatTimer;

  // 34 bits + 34 pauses + 1 stop value (0)
  //int PulseValues[34*2+1];
  int *PulseValues;
}
heliData;

struct eepromValues_t
{
  byte dataInit;
  bool joystickEnabled;
  byte heliType;
  byte shieldType;
  byte CH;
  byte MAP[4];
  byte invert[4];
}
eepromValues;

int heli_code_checksum[200];
char heli_codes[36];

heliData Helis[4];

#define inSerialSize 100
char inSerialBuffer[inSerialSize];           // a string to hold incoming data
int inSerialRead = 0;
int inSerialWrite = 0;
boolean inSerialComplete = false; // whether the string is complete

int Zero[4];
int AD[4];

void setup()
{
  initSerial();

  pinMode(IR_PIN, OUTPUT);
  digitalWrite(IR_PIN, IR_OFF);

  for (int i = 2; i < 6; i++)
  {
    pinMode(i, OUTPUT);
    digitalWrite(i, HIGH);
    delay(500);
    digitalWrite(i, LOW);
  }

  pinMode(6, INPUT);
  pinMode(7, INPUT);

  for (int i = 14; i < 18; i++)
  {
    pinMode(i, INPUT);
    digitalWrite(i, LOW);
  }

  for (int i = 0; i < 4; i++)
  {
    Zero[i] = analogRead(14 + i);
  }

  // read eeprom data
  eeprom_read_block((void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));
  // initialize to default values if data seems corrupted
  if (eepromValues.dataInit != 17)
  {
    eepromValues.dataInit = 17;
    eepromValues.joystickEnabled = false;

    eepromValues.shieldType = 1;
    //eepromValues.shieldType=2;

    //eepromValues.heliType=TURBOHAWK;
    //eepromValues.heliType=TWOCH;
    eepromValues.heliType = UFO;

    eepromValues.CH = CHA;

    //physical signals:
    //AD[0]-left joystick, left-right, left=1023, right=0;
    //AD[1]-left joystick, up-down, up=0, dn=1023;
    //AD[2]-right joystick, left-right, left=1023, right=0;
    //AD[3]-right joystick, up-down, up=0, dn=1023;

    eepromValues.MAP[0] = 1; // throttle, CH 1
    eepromValues.MAP[1] = 2; // rudder, CH 0
    eepromValues.MAP[2] = 3; // pitch, CH 3
    eepromValues.MAP[3] = 0; // trim, set to CH2.

    for (int i = 0; i < 4; i++)
    {
      eepromValues.invert[i] = false;
    }

    eeprom_write_block((const void*)&eepromValues, (void*)EEPROMADD, sizeof(eepromValues));
  }

  for (int i = 0; i < 4; i++)
  {
    initHeli(&Helis[i], 0);
  }

  Serial.println(F("IR Heli Controller"));
  Serial.print(F("Shield Version: "));
  Serial.println(eepromValues.shieldType);

  if (eepromValues.joystickEnabled)
  {
    Serial.println(F("Joystick Enabled"));
    Serial.print(F("Heli Type: "));
    Serial.println(eepromValues.heliType);

    initController();
  }

  Serial.println(F("Ready to accept commands, ?; for help"));

  //Ram_TableDisplay();
}

int ABbuttoncounter = 0;
unsigned long ABpress = 0;

void loop()
{
  if (inSerialComplete || !inBufferFree())
  {
    processResponses();
    inSerialComplete = false;
  }

  //physical signals for shield1:
  //AD[0]-left joystick, left-right, left=1023, right=0;
  //AD[1]-left joystick, up-down, up=0, dn=1023;
  //AD[2]-right joystick, left-right, left=1023, right=0;
  //AD[3]-right joystick, up-down, up=0, dn=1023;

  //physical signals for shield2:
  //AD[0]-left joystick, left-right, left=1023, right=0;
  //AD[1]-left joystick, up-down, up=1023, dn=0;
  //AD[2]-right joystick, left-right, left=1023, right=0;
  //AD[3]-right joystick, up-down, up=1023, dn=0;

  for (int i = 0; i < 4; i++)
  {
    AD[i] = analogRead(14 + i);

    if (eepromValues.invert[i])
    {
      AD[i] = 1023 - AD[i];
    }
  }

  if (eepromValues.shieldType == 2)
  {
    AD[1] = 1023 - AD[1];
    AD[3] = 1023 - AD[1];
  }

  if (eepromValues.joystickEnabled)
  {
    int throttle = AD[eepromValues.MAP[0]] + (512 - Zero[eepromValues.MAP[0]]);
    int rudder = AD[eepromValues.MAP[1]] + (512 - Zero[eepromValues.MAP[1]]);
    int pitch = AD[eepromValues.MAP[2]] + (512 - Zero[eepromValues.MAP[2]]);
    int trim = AD[eepromValues.MAP[3]] + (512 - Zero[eepromValues.MAP[3]]);

    int left = digitalRead(6);
    int right = digitalRead(7);

    if ((right == 0) && (millis() - ABpress > 1000))
    {
      ABpress = millis();
      //0..3
      ABbuttoncounter = (ABbuttoncounter + 1) & 3;

      for (int i = 2; i < 6; i++)
      {
        digitalWrite(i, LOW);
      }

      switch (ABbuttoncounter)
      {
        case 1:
          setABit(&Helis[0], 0);
          setBBit(&Helis[0], 0);
          digitalWrite(2, HIGH);
          break;
        case 2:
          setABit(&Helis[0], 0);
          setBBit(&Helis[0], 1);
          digitalWrite(3, HIGH);
          break;
        case 3:
          setABit(&Helis[0], 1);
          setBBit(&Helis[0], 0);
          digitalWrite(4, HIGH);
          break;
        case 0:
          setABit(&Helis[0], 1);
          setBBit(&Helis[0], 1);
          digitalWrite(5, HIGH);
          break;
      }
    }

    /*
    Serial.print(F(" t: "));
     Serial.print(throttle);
     Serial.print(F(" r: "));
     Serial.print(rudder);
     Serial.print(F(" p: "));
     Serial.print(pitch);
     */

    throttle = (100 - (throttle / 1024.0) * 100) - 50;
    throttle = (throttle < 0) ? 0 : throttle;
    throttle *= 2;

    rudder = 100 - (rudder / 1024.0) * 100;
    pitch = 100 - (pitch / 1024.0) * 100;
    trim = 100 - (trim / 1024.0) * 100;

    /*
    Serial.print(F(" t: "));
     Serial.print(throttle);
     Serial.print(F(" r: "));
     Serial.print(rudder);
     Serial.print(F(" p: "));
     Serial.print(pitch);

     Serial.println();
     */

    setThrottlePercent(&Helis[0], throttle);
    setRudderPercent(&Helis[0], rudder);
    setPitchPercent(&Helis[0], pitch);
    setTrimPercent(&Helis[0], trim);
  }

  for (int i = 0; i < 4; i++)
  {
    sendTiming(&Helis[i]);
  }
}

void initController()
{
  //BUG: read from EEPROM
  initHeli(&Helis[0], eepromValues.heliType);
  setCH(&Helis[0], eepromValues.CH);
  bindHeli(&Helis[0]);
  setThrottlePercent(&Helis[0], 0);
}

void stopController()
{
  setThrottlePercent(&Helis[0], 0);
  for (int i = 0; i < 4; i++)
  {
    sendTiming(&Helis[0]);
  }
  Helis[0].Init = 0;
}

//************************************************************************
//*	http://www.nongnu.org/avr-libc/user-manual/malloc.html
//*	thanks to John O for the pointer to this info and the insperation to do it
void	Ram_TableDisplay(void)
{
  char stack = 1;
  extern char *__data_start;
  extern char *__data_end;

  extern char *__bss_start;
  extern char *__bss_end;
  //extern char *__brkval;
  extern char *__heap_start;
  extern char *__heap_end;
  //extern char *__malloc_heap_end;
  //extern size_t __malloc_margin;


  int	data_size	=	(int)&__data_end - (int)&__data_start;
  int	bss_size	=	(int)&__bss_end - (int)&__data_end;
  int	heap_end	=	(int)&stack - (int)&__malloc_margin;
  //	int	heap_size	=	(int)__brkval - (int)&__bss_end;
  int	heap_size	=	heap_end - (int)&__bss_end;
  int	stack_size	=	RAMEND - (int)&stack + 1;
  int	available	=	(RAMEND - (int)&__data_start + 1);

  available	-=	data_size + bss_size + heap_size + stack_size;

  Serial.println(F("CPU USAGE"));

  Serial.print(F("+----------------+  __data_start  ="));
  Serial.println((int)&__data_start);
  Serial.print(F("+      data      +"));
  Serial.println();
  Serial.print(F("+    variables   +  data_size     ="));
  Serial.println(data_size);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+----------------+  __data_end    ="));
  Serial.println((int)&__data_end);
  Serial.print(F("+----------------+  __bss_start   ="));
  Serial.println((int)&__bss_start);
  Serial.print(F("+       bss      +"));
  Serial.println();
  Serial.print(F("+    variables   +  bss_size      ="));
  Serial.println(bss_size);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+----------------+  __bss_end     ="));
  Serial.println((int)&__bss_end);
  Serial.print(F("+----------------+  __heap_start  ="));
  Serial.println((int)&__heap_start);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+       heap     +  heap_size     ="));
  Serial.println(heap_size);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+----------------+  heap_end      ="));
  Serial.println(heap_end);
  Serial.print(F("+----------------+  Current STACK ="));
  Serial.println((int)&stack);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+      stack     +  stack_size    ="));
  Serial.println(stack_size);
  Serial.print(F("+                +"));
  Serial.println();
  Serial.print(F("+----------------+  RAMEND        ="));
  Serial.println(RAMEND);

  //	Serial.print(F("__brkval      ="));
  //	Serial.println((int)__brkval);

  Serial.print(F("available = "));
  Serial.println(available);

  Serial.println();
  Serial.print(F("Free memory "));
  Serial.println(get_free_memory());
}

int get_free_memory()
{
  extern char __bss_end;
  extern char *__brkval;

  int free_memory;

  if ((int)__brkval == 0)
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  else
    free_memory = ((int)&free_memory) - ((int)__brkval);

  return free_memory;
}
