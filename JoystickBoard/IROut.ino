//
// file: IROut.ino
//

/*
 void initHeli(struct heliData_t *heli, byte heliType)
 
 showHeliState(struct heliData_t *heli)
 
 void setChecksum(struct heliData_t *heli)
 
 void setThrottlePercent(struct heliData_t *heli, byte throttle)
 void setThrottleBits(struct heliData_t *heli, byte throttle)
 
 void setRudderPercent(struct heliData_t *heli, byte rudder)
 void setRudderBits(struct heliData_t *heli, byte rudder)
 
 void setPitchPercent(struct heliData_t *heli, byte pitch)
 void setPitchBits(struct heliData_t *heli, byte pitch)
 
 void setCH(struct heliData_t *heli, byte ch) 
 void setCHBits(struct heliData_t *heli, byte ch)
 
 void setTrimPercent(struct heliData_t *heli, byte trim)
 void setTrimBits(struct heliData_t *heli, byte trim)
 
 void setABit(struct heliData_t *heli, byte a)
 void setBBit(struct heliData_t *heli, byte b)
 
 void bindHeli(struct heliData_t *heli)
 
 void setBits(struct heliData_t *heli, byte pos, byte len, byte value)
 
 void sendTiming(struct heliData_t *heli)
 void sendBit(long on_time,long off_time)
 */

void initHeli(struct heliData_t *heli, byte heliType)
{
  heli->Init=0;
  heli->Bound=false;
  heli->HeliType=0;
  heli->Throttle=0;
  heli->Rudder=0;
  heli->Pitch=0;
  heli->CH=0;
  heli->Trim=0;
  heli->A=0;
  heli->B=0;
  heli->Check=0;

  heli->Header=0;
  heli->One=0;
  heli->Zero=0;
  heli->Off=0;  

  heli->RepeatTimer=0;    

  heli->PulseValues=0;

  switch(heliType)
  {
  case TURBOHAWK:
    heli->Init=HELIINIT;
    heli->HeliType=heliType;

    heli->Header=TURBOHAWK_HEADER;
    heli->One=TURBOHAWK_ONE;
    heli->Zero=TURBOHAWK_ZERO;
    heli->Off=TURBOHAWK_OFF;

    heli->RepeatRate=TURBOHAWK_REPEAT;

    heli->PulseValues=new int[34*2+1];
    for (int i=0; i<34*2+1; i++)
    {
      (heli->PulseValues)[i]=0;
    }

    (heli->PulseValues)[0]=heli->Zero;
    (heli->PulseValues)[1]=heli->Off;
    (heli->PulseValues)[2]=heli->Header;
    (heli->PulseValues)[3]=heli->Off;

    setThrottlePercent(heli,0);
    setRudderPercent(heli,50);
    setPitchPercent(heli,50);
    setCHBits(heli,heli->CH);
    setTrimPercent(heli,50);
    setABit(heli,heli->A);
    setBBit(heli,heli->B);

    break;

  case TWOCH:
    heli->Init=HELIINIT;
    heli->Bound=true;
    heli->HeliType=heliType;

    heli->Header=TWOCH_HEADER;
    heli->One=TWOCH_ONE;
    heli->Zero=TWOCH_ZERO;
    heli->Off=TWOCH_OFF;    

    heli->RepeatRate=TWOCH_REPEAT;

    heli->PulseValues=new int[13*2+1];
    for (int i=0; i<13*2+1; i++)
    {
      (heli->PulseValues)[i]=0;
    }

    (heli->PulseValues)[0]=heli->Header;
    (heli->PulseValues)[1]=heli->Off;

    setCHBits(heli,heli->CH);
    setThrottlePercent(heli,0);
    setRudderPercent(heli,50);
    setTrimPercent(heli,50);

    break;

  case UFO:
    heli->Init=HELIINIT;
    heli->Bound=true;
    heli->HeliType=heliType;

    heli->Header=UFO_HEADER;
    heli->One=UFO_ONE;
    heli->Zero=UFO_ZERO;
    heli->Off=UFO_OFF;    

    heli->RepeatRate=UFO_REPEAT;

    heli->PulseValues=new int[14*2+1];

    int j=0;
    for (int i=0; i<14; i++)
    {
      (heli->PulseValues)[j++]=heli->Zero;
      (heli->PulseValues)[j++]=heli->Off;      
    }
    (heli->PulseValues)[j]=0;

    (heli->PulseValues)[0]=heli->Header;
    (heli->PulseValues)[1]=heli->Off;

    setCHBits(heli,heli->CH);
    setThrottlePercent(heli,0);

    break;    

  }  
}

void showHeliState(struct heliData_t *heli)
{
  Serial.print(F("Init:"));
  Serial.println(heli->Init);
  Serial.print(F("Bound:"));
  Serial.println(heli->Bound);
  Serial.print(F("HeliType:"));
  Serial.println(heli->HeliType);
  Serial.print(F("Throttle:"));
  Serial.println(heli->Throttle);
  Serial.print(F("Rudder:"));
  Serial.println(heli->Rudder);
  Serial.print(F("Pitch:"));
  Serial.println(heli->Pitch);
  Serial.print(F("CH:"));
  Serial.println(heli->CH);
  Serial.print(F("Trim:"));
  Serial.println(heli->Trim);
  Serial.print(F("A:"));
  Serial.println(heli->A);
  Serial.print(F("B:"));
  Serial.println(heli->B);
  Serial.print(F("Check:"));
  Serial.println(heli->Check);    

  Serial.print(F("Header:"));
  Serial.println(heli->Header);
  Serial.print(F("One:"));
  Serial.println(heli->One);
  Serial.print(F("Zero:"));
  Serial.println(heli->Zero);
  Serial.print(F("Off:"));
  Serial.println(heli->Off);

  Serial.print(F("PulseValues:"));

  if (heli->PulseValues!=0)
  {
    for (int j=0; j<34*2+1; j++)
    {
      int v=(heli->PulseValues)[j];

      if (v==0)
      {
        Serial.print(F("Z"));
      } 
      else
        if (v==heli->Header)
        {
          Serial.print(F("H"));
        } 
        else
          if (v==heli->One)
          {
            Serial.print(F("1"));
          } 
          else
            if (v==heli->Zero)
            {
              Serial.print(F("0"));
            } 
            else
              if (v==heli->Off)
              {
                Serial.print(F("P"));
              } 
              else
              {
                Serial.print(F("E"));
              }
    }
  }
  else
  {
    Serial.print(F("Not Initialized"));
  }
  Serial.println(F(""));    
}

void setChecksum(struct heliData_t *heli)
{
  byte checksum;

  if (heli->Init!=HELIINIT)
  {
    return;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    checksum=(heli->Throttle + heli->Pitch + heli->Trim + (heli->Rudder<<4)) & 63;
    heli->Check=checksum;
    setBits(heli,28*2,6,checksum);
    break;
  }    

}

void setThrottlePercent(struct heliData_t *heli, byte throttle)
{
  if (throttle>100)
  {
    throttle=100;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:    
    setThrottleBits(heli,(throttle/(double)100)*85);
    break;

  case TWOCH:   
    setThrottleBits(heli,(throttle/(double)100)*31);
    break;

  case UFO:   
    setThrottleBits(heli,(throttle/(double)100)*31);
    break;    
  }  
}

void setThrottleBits(struct heliData_t *heli, byte throttle)
{
  if (heli->Init!=HELIINIT)
  {
    return;
  }

  heli->Throttle=throttle;

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    setBits(heli,2*2,8,throttle);
    break;

  case TWOCH:   
    setBits(heli,3*2,5,throttle);
    break;

  case UFO:   
    setBits(heli,3*2,5,throttle);
    break;    
  }  

  setChecksum(heli);
}

void setRudderPercent(struct heliData_t *heli, byte rudder)
{
  if (rudder>100)
  {
    rudder=100;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:    
    //0 MIN, 14 MAX, 50% NEEDS TO BE 8...
    rudder=(rudder/(double)100)*16;
    rudder=(rudder>14)?14:rudder;
    setRudderBits(heli,rudder);
    break;

  case TWOCH:   
    setRudderBits(heli,(rudder/(double)100)*6);
    break;
  }   
}

void setRudderBits(struct heliData_t *heli, byte rudder)
{
  if (heli->Init!=HELIINIT)
  {
    return;
  }  

  heli->Rudder=rudder;

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    setBits(heli,10*2,4,rudder);
    break;

  case TWOCH:
    setBits(heli,8*2,3,rudder);
    break;
  }

  setChecksum(heli);
}

void setPitchPercent(struct heliData_t *heli, byte pitch)
{
  if (pitch>100)
  {
    pitch=100;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:    
    setPitchBits(heli,(pitch/(double)100)*15);
    break;
  }   
}

void setPitchBits(struct heliData_t *heli, byte pitch)
{
  if (heli->Init!=HELIINIT)
  {
    return;
  }  

  heli->Pitch=pitch;

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    setBits(heli,14*2,4,pitch);
    break;
  }

  setChecksum(heli);
}

void setCH(struct heliData_t *heli, byte ch)
{  
  byte channel=0;

  if (heli->Init!=HELIINIT)
  {
    return;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    switch (ch)
    {
    case CHA:
      channel=TURBOHAWK_CHA;
      break;
    case CHB:
      channel=TURBOHAWK_CHB;
      break;
    case CHC:
      channel=TURBOHAWK_CHC;
      break;
    default:
      channel=TURBOHAWK_CHA;
      break;
    }
    break;

  case TWOCH:
    switch (ch)
    {
    case CHA:
      channel=TWOCH_CHA;
      break;
    case CHB:
      channel=TWOCH_CHB;
      break;
    case CHC:
      channel=TWOCH_CHC;
      break;
    default:
      channel=TWOCH_CHA;
      break;
    }
    break;

  case UFO:
    switch (ch)
    {
    case CHA:
      channel=UFO_CHA;
      break;
    case CHB:
      channel=UFO_CHB;
      break;
    case CHC:
      channel=UFO_CHC;
      break;
    default:
      channel=UFO_CHA;
      break;
    }
    break;    
  } 

  setCHBits(heli, channel);
}

void setCHBits(struct heliData_t *heli, byte ch)
{  
  switch(heli->HeliType)
  {
  case TURBOHAWK:    
    setBits(heli,18*2,2,ch);
    break;

  case TWOCH:
    setBits(heli,1*2,2,ch);
    break;

  case UFO:
    setBits(heli,1*2,2,ch);
    break;    
  } 

  if (heli->CH!=ch)
  {
    heli->Bound=false;
  }

  heli->CH=ch;

  setChecksum(heli);
}

void setTrimPercent(struct heliData_t *heli, byte trim)
{
  if (trim>100)
  {
    trim=100;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:    
    setTrimBits(heli,(trim/(double)100)*44);
    break;

  case TWOCH:   
    setTrimBits(heli,(trim/(double)100)*2);
    break;
  }   
}

void setTrimBits(struct heliData_t *heli, byte trim)
{
  if (heli->Init!=HELIINIT)
  {
    return;
  }  

  heli->Trim=trim;

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    setBits(heli,20*2,6,trim);
    break;

  case TWOCH:
    setBits(heli,11*2,2,trim);
    break;
  }

  setChecksum(heli);
}

/*
void setABBits(struct heliData_t *heli, byte ab)
 {  
 if (heli->Init!=HELIINIT)
 {
 return;
 }
 
 heli->AB=ab;
 
 switch(heli->HeliType)
 {
 case TURBOHAWK:
 setBits(heli,26*2,2,ab);
 break;
 } 
 
 setChecksum(heli);
 }
 */

void setABit(struct heliData_t *heli, byte a)
{  
  if (heli->Init!=HELIINIT)
  {
    return;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    heli->A=a;
    setBits(heli,26*2,1,a);
    break;
  case UFO:
    heli->A=a;
    setBits(heli,9*2,1,a);
    break;
  } 

  setChecksum(heli);
}

void setBBit(struct heliData_t *heli, byte b)
{  
  if (heli->Init!=HELIINIT)
  {
    return;
  }

  switch(heli->HeliType)
  {
  case TURBOHAWK:
    heli->B=b;
    setBits(heli,27*2,1,b);
    break;
  case UFO:
    heli->B=b;
    setBits(heli,12*2,1,b);
    break;
  } 

  setChecksum(heli);
}



void bindHeli(struct heliData_t *heli)
{  
  if (heli->Init!=HELIINIT)
  {
    return;
  }  

  if(heli->HeliType==TURBOHAWK)
  {
    //stringToTiming does not update variables in struct, checksum will fail if changed...
    //stringToTiming(heli, "0H10000000100010000010100100110001");
    //stringToTiming(heli, "0H10000000000000000000000000000000");

    //all that seems to be needed for binding is throttle=128
    setThrottleBits(heli,128); 
    //original remote also sets rudder & pitch to 8, but heli binds when moving throttle down to idle with rudder & pitch stick off center
    //setRudderBits(heli,8);
    //setPitchBits(heli,8);
  }

  if(heli->HeliType==UFO)
  {
    setThrottleBits(heli,0); 
  }

  sendTiming(heli);
  delay(5);
  sendTiming(heli);
  delay(5);
  sendTiming(heli);
  delay(5);
  sendTiming(heli);

  heli->Bound=true;
}

void setBits(struct heliData_t *heli, byte pos, byte len, byte value)
{
  byte b;  
  int t_ndx=pos;

  if (heli->Init!=HELIINIT)
  {
    return;
  }   

  for (int i = len-1; i >=0; i--)
  {
    b = ((value) & (1 << i)) >> i;    
    (heli->PulseValues)[t_ndx++]=(b>0)?heli->One:heli->Zero;
    (heli->PulseValues)[t_ndx++]=heli->Off;      
  }
}

void stringToTiming(struct heliData_t *heli, char str[])
{
  int c_ndx=0;
  int t_ndx=0;
  int t=0;
  char c=1;

  if (heli->Init!=HELIINIT)
  {
    return;
  }

  do
  {
    c = str[c_ndx++];

    if (c!=0)
    {
      switch (c)
      {
      case 'H':
        t=heli->Header;
        break;
      case '0':
        t=heli->Zero;
        break;
      case '1':
        t=heli->One;
        break;
      }

      (heli->PulseValues)[t_ndx++]=t;
      (heli->PulseValues)[t_ndx++]=heli->Off;
    }
  } 
  while (c!=0);

  (heli->PulseValues)[t_ndx]=0;

}

void sendTiming(struct heliData_t *heli)
{
  int t_ndx=0;
  int on_time,off_time;
  unsigned long timenow;

  if (heli->Init!=HELIINIT)
  {
    return; // exit function.
  }

  timenow=micros();
  
  if ((heli->RepeatTimer==0) || (timenow - heli->RepeatTimer > heli->RepeatRate))
  {
    /*
    Serial.print(heli->HeliType);
    Serial.print(F(" "));
    Serial.println(timenow - heli->RepeatTimer);
    */

    heli->RepeatTimer=timenow;
    
    do
    {
      on_time=(heli->PulseValues)[t_ndx++];
      off_time=(heli->PulseValues)[t_ndx++];

      if (on_time==0)
      {
        break; // exit loop
      }

      sendBit(on_time,off_time);
    } 
    while (true);
  }
  
  //BUG: eliminate delay, use global index and time of last transmission
  delay(5);
}

void sendBit(long on_time,long off_time)
{
  /*
  Serial.print(on_time);
   Serial.print(F(" "));
   Serial.println(off_time);
   */

  cli();  // this turns off any background interrupts

  while (on_time > 0) {
    // 38 kHz is about 13 microseconds high and 13 microseconds low
    digitalWrite(IR_PIN, IR_ON);           // this takes about 3 microseconds to happen
    delayMicroseconds(DELAY_38kH);         // hang out for 10 microseconds, you can also change this to 9 if its not working
    digitalWrite(IR_PIN, IR_OFF);          // this also takes about 3 microseconds
    delayMicroseconds(DELAY_38kH);         // hang out for 10 microseconds, you can also change this to 9 if its not working

    // so 26 microseconds altogether
    on_time -= 26;
  }

  sei();  // this turns them back on

  delayMicroseconds(off_time);
}
