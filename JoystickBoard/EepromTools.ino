//
// file: EepromTools.ino
//

// start reading from the first byte (address 0) of the EEPROM
int MS5_address = 0;
byte MS5_value;

void MS5_setup()
{
  MS5_printMenu();    
}

void MS5_printMenu()
{
  Serial.println(F("EEPROM Tools v0.1.27"));
  Serial.println(F(""));
  Serial.println(F(" 0 - Zero EEPROM"));
  Serial.println(F(" 1 - Fill EEPROM"));
  Serial.println(F(" 3 - Print EEPROM"));
  Serial.println(F(""));  
  Serial.println(F("Enter Choice (0,1,3), any key during test cancels: "));      
}

void MS5_zeroEEPROM()
{
  int i;
  int confirm;
  
  Serial.println(F("All EEPROM cells will be zeroed "));
  Serial.println(F("Are you sure (Y/N):"));

  do
  {
  }
  while (!Serial.available());      
  confirm = Serial.read();

  if ((confirm=='Y') || (confirm=='y'))
  {
    Serial.println(F(""));
    Serial.println(F("Zeroing EEPROM"));  

    for (i=0;i<1024;i++)
    {
      EEPROM.write(i, 0);
    }

    Serial.println(F("Done...")); 
    Serial.println(F(""));
  }
}

void MS5_fillEEPROM()
{
  int i;
  int confirm;
  
  Serial.println(F("All EEPROM cells will be set to 0xFF "));
  Serial.println(F("Are you sure (Y/N):"));

  do
  {
  }
  while (!Serial.available());      
  confirm = Serial.read();

  if ((confirm=='Y') || (confirm=='y'))
  {

    Serial.println(F(""));
    Serial.println(F("Filling EEPROM with 0xFF"));  

    for (i=0;i<1024;i++)
    {
      EEPROM.write(i, 255);
    }

    Serial.println(F("Done...")); 
    Serial.println(F(""));
  }
}

void MS5_printEEPROM()
{
  int i,j,v;
  int add=0;
  int line=0;

  Serial.println(F(""));

  for (int i=0; i<64; i++)
  {
    if (add==0)
    {
      Serial.print(F("000"));
    }
    else
    {
      if (add<112)
      {
        Serial.print(F("00"));
      }
      else
      {
        if (add<1008)
        {
          Serial.print(F("0"));
        }
      }
    }

    Serial.print(add);
    Serial.print(F(" "));

    line=add;

    for (int j=0; j<16; j++)
    {
      v = EEPROM.read(add);
      add++;
      Serial.print(v);
      Serial.print(F(" "));
    }

    Serial.print(F(" "));

    add=line;

    for (int j=0; j<16; j++)
    {
      v = EEPROM.read(add);
      add++;

      if ((v<31) || (v==127))
      {
        v = 32;
      }

      Serial.write(v);
    } 

    Serial.println(F(""));
  }

  Serial.println(F(""));
}

void MS5_loop()
{
  if (Serial.available())
  {
    switch(Serial.read())
    {
    case '0':
      MS5_zeroEEPROM();
      break;

    case '1':
      MS5_fillEEPROM();
      break;

    case '3':
      MS5_printEEPROM();
      break;
    }

    MS5_printMenu();
  }

}



