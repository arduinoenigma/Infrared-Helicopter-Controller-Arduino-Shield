/* Raw IR commander
 
 IR Dome...
 3.5mm stereo plug  : The Tip is VCC(3.3V-5V),the Ring is Data(IR signal),the Base is GND.
 
 This sketch/program uses the Arduno and a PNA4602 to 
 decode IR received.  It then attempts to match it to a previously
 recorded IR signal
 
 Code is public domain, check out www.ladyada.net and adafruit.com
 for more tutorials! 
 */

// We need to use the 'raw' pin reading methods
// because timing is very important here and the digitalRead()
// procedure is slower!
//uint8_t IRpin = 2;
// Digital pin #2 is the same as Pin D2 see
// BUG: CHANGED TO Analog 4 -> PIN C4
// http://arduino.cc/en/Hacking/PinMapping168 for the 'raw' pin mapping
#define IRpin_PIN      PINC
#define IRpin          4

// the maximum pulse we'll listen for - 65 milliseconds is a long time
#define MAXPULSE 65000
#define NUMPULSES 50

// what our timing resolution should be, larger is better
// as its more 'precise' - but too large and you wont get
// accurate timing
#define RESOLUTION 10 

// What percent we will allow in variation to match the same code
#define FUZZINESS 20

// we will store up to 100 pulse pairs (this is -a lot-)
uint16_t pulses[NUMPULSES][2];  // pair is high and low pulse 
uint8_t currentpulse = 0; // index for pulses we're storing

char checksum1[6];
char heli_codes[36];
int  heli_codes_index=0;
int  freeRam=2048;
int  heli_codes_checksum_index=0;
unsigned int heli_code_checksum[200];
int  dummy=36;

#include "ircodes.h"
#include <MemoryFree.h>

void setup(void) {
  Serial.begin(9600);

  delay(500);

  Serial.println("1 - Show Raw Pulses");
  Serial.println("2 - Decode incoming Protocol Turbo Hawk packets");
  Serial.println("3 - Show Turbo Hawk C code");
  Serial.println("4 - Decode 2CH Protocol");    
  Serial.println("");
  Serial.println("Mode: 4");    

  Serial.println("Ready to decode IR!");
}

char incomingByte='4';

void loop(void) {

  int numberpulses;

  if (Serial.available() > 0)
  {
    incomingByte = Serial.read();  

    switch (incomingByte)
    {
    case '1':    
      Serial.println("\n1 - Show Raw Pulses");
      break;

    case '2':
      Serial.println("\n2 - Decode incoming Protocol Turbo Hawk packets");    
      break;      

    case '3':
      Serial.println("\n3 - Show Turbo Hawk C code");    
      break;

    case '4':
      Serial.println("\n4 - Decode 2CH Protocol");    
      break;

    default:
      Serial.println("\ninvalid command...");
      Serial.println("1 - Show Raw Pulses");
      incomingByte='1';
      break;
    }
  }

  numberpulses = listenForIR();

  switch (incomingByte)
  {
  case '1':
    Serial.print("Heard ");
    Serial.print(numberpulses);
    Serial.println("-pulse long IR signal");
    printpulses();
    break;

  case '2':
    printTHHeliLine();
    break;

  case '3':
    printTHHeliCode();
    break;

  case '4':
    print2CHHeliLine();
    break;
  }

  if (dummy!=36)
  {
    Serial.println("MEMORY CORRUPTION!");
  }

  /*
  if (IRcompare(numberpulses, ApplePlaySignal,sizeof(ApplePlaySignal)/4)) {
   Serial.println("PLAY");
   }
   if (IRcompare(numberpulses, AppleRewindSignal,sizeof(AppleRewindSignal)/4)) {
   Serial.println("REWIND");
   }
   if (IRcompare(numberpulses, AppleForwardSignal,sizeof(AppleForwardSignal)/4)) {
   Serial.println("FORWARD");
   }
   delay(500);
   */
}

//KGO: added size of compare sample. Only compare the minimum of the two
boolean IRcompare(int numpulses, int Signal[], int refsize) {
  int count = min(numpulses,refsize);
  Serial.print("count set to: ");
  Serial.println(count);
  for (int i=0; i< count-1; i++) {
    int oncode = pulses[i][1] * RESOLUTION / 10;
    int offcode = pulses[i+1][0] * RESOLUTION / 10;

#ifdef DEBUG    
    Serial.print(oncode); // the ON signal we heard
    Serial.print(" - ");
    Serial.print(Signal[i*2 + 0]); // the ON signal we want 
#endif   

    // check to make sure the error is less than FUZZINESS percent
    if ( abs(oncode - Signal[i*2 + 0]) <= (Signal[i*2 + 0] * FUZZINESS / 100)) {
#ifdef DEBUG
      Serial.print(" (ok)");
#endif
    } 
    else {
#ifdef DEBUG
      Serial.print(" (x)");
#endif
      // we didn't match perfectly, return a false match
      return false;
    }


#ifdef DEBUG
    Serial.print("  \t"); // tab
    Serial.print(offcode); // the OFF signal we heard
    Serial.print(" - ");
    Serial.print(Signal[i*2 + 1]); // the OFF signal we want 
#endif    

    if ( abs(offcode - Signal[i*2 + 1]) <= (Signal[i*2 + 1] * FUZZINESS / 100)) {
#ifdef DEBUG
      Serial.print(" (ok)");
#endif
    } 
    else {
#ifdef DEBUG
      Serial.print(" (x)");
#endif
      // we didn't match perfectly, return a false match
      return false;
    }

#ifdef DEBUG
    Serial.println();
#endif
  }
  // Everything matched!
  return true;
}

int listenForIR(void) {
  currentpulse = 0;

  while (1) {
    uint16_t highpulse, lowpulse;  // temporary storage timing
    highpulse = lowpulse = 0; // start out with no pulse length

    //  while (digitalRead(IRpin)) { // this is too slow!
    while (IRpin_PIN & (1 << IRpin)) {
      // pin is still HIGH

      // count off another few microseconds
      highpulse++;
      delayMicroseconds(RESOLUTION);

      // If the pulse is too long, we 'timed out' - either nothing
      // was received or the code is finished, so print what
      // we've grabbed so far, and then reset

      // KGO: Added check for end of receive buffer
      if (((highpulse >= MAXPULSE) && (currentpulse != 0))|| currentpulse == NUMPULSES) {
        return currentpulse;
      }
    }
    // we didn't time out so lets stash the reading
    pulses[currentpulse][0] = highpulse;

    // same as above
    while (! (IRpin_PIN & _BV(IRpin))) {
      // pin is still LOW
      lowpulse++;
      delayMicroseconds(RESOLUTION);
      // KGO: Added check for end of receive buffer
      if (((lowpulse >= MAXPULSE)  && (currentpulse != 0))|| currentpulse == NUMPULSES) {
        return currentpulse;
      }
    }
    pulses[currentpulse][1] = lowpulse;

    // we read one high-low pulse successfully, continue!
    currentpulse++;
  }
}

void printpulses(void) {
  Serial.println("\n\r\n\rReceived: \n\rOFF \tON");
  for (uint8_t i = 0; i < currentpulse; i++) {
    Serial.print(pulses[i][0] * RESOLUTION, DEC);
    Serial.print(" usec, ");
    Serial.print(pulses[i][1] * RESOLUTION, DEC);
    Serial.println(" usec");
  }

  // print it in a 'array' format
  Serial.println("int IRsignal[] = {");
  Serial.println("// ON, OFF (in 10's of microseconds)");
  for (uint8_t i = 0; i < currentpulse-1; i++) {
    Serial.print("\t"); // tab
    Serial.print(pulses[i][1] * RESOLUTION / 10, DEC);
    Serial.print(", ");
    Serial.print(pulses[i+1][0] * RESOLUTION / 10, DEC);
    Serial.println(",");
  }
  Serial.print("\t"); // tab
  Serial.print(pulses[currentpulse-1][1] * RESOLUTION / 10, DEC);
  Serial.print(", 0};");
}

void printTHHeliCode(void) {

  bool enable = false;

  for (uint8_t i = 0; i < currentpulse-1; i++) {

    bool error=true;
    int v = pulses[i][1];

    if (enable)
    {
      if ((v>35) && (v<45))
      {
        Serial.println("send1(ZERO);");
        error=false; 
      }

      if ((v>75) && (v<85))
      {
        Serial.println("send1(ONE);");
        error=false;         
      }        
    }

    if (v>145)
    {
      if (!enable)
      {
        enable = true;
        error=false;         

        Serial.println("");
        Serial.println("--------");

        Serial.println("send1(ZERO);");
        Serial.println("send1(HEADER);");
      }
      else
      {
        enable = false;
      }
    }

    if (enable && error)
    {
      Serial.print("ERROR IN BIT:");
      Serial.println(v);
    }

  }
}

void printTHHeliLine(void) 
{
  bool enable = false;
  heli_codes_index=0;

  for (uint8_t i = 0; i < currentpulse-1; i++) 
  {
    bool error=true;
    int v = pulses[i][1];

    if ((heli_codes_index==36) && enable)
    {
      enable=false;
    }

    if (enable)
    {
      if ((v>35) && (v<45))
      {
        //Serial.print("0");
        heli_codes[heli_codes_index++]='0';
        error=false; 
      }

      if ((v>75) && (v<85))
      {
        //Serial.print("1");        
        heli_codes[heli_codes_index++]='1';
        error=false;         
      }        
    }

    if (v>145)
    {
      if (!enable)
      {
        //Serial.print("0H")
        heli_codes_index=2;
        enable = true;
        error=false;       
        heli_codes[0]='0';
        heli_codes[1]='H';
        ;
      }
      else
      {
        if (heli_codes_index==35)  // detect missed pulses between header pulses, do not print short frame...
        {
          //BUG: change this to only output bits
          //uniqueTHHeliLine()
          //BUG: this outputs bits & decodes them
          uniqueTHHeliLineDecoded();
        }
        heli_codes_index=0;
        enable = false;
      }
    }

    if (enable && error)  // if we are detecting, always include something, 1, 0 or E, header starts/stops detection
    {
      //Serial.print("E");
      heli_codes[heli_codes_index++]='E';
      //PATCH: stop capture, comment line below to print line with E
      enable = false;
    }
  }
}

void uniqueTHHeliLine()
{
  unsigned int checksum=0;
  unsigned int weight=1;
  bool found=false;
  char v=0;

  //detectFreeRam();
  //Serial.println(freeRam);

  for (uint8_t i = 0; i < 34; i++) 
  {
    v=heli_codes[i]-'0';
    if (v>1)
    {
      v=0;
    }

    checksum+=v*weight;
    weight+=117;
  }

  for (uint8_t i = 0; i < heli_codes_checksum_index; i++)
  {
    if (heli_code_checksum[i]==checksum)
    {
      found=true;
      break;
    }
  } 

  if (!found)
  {
    heli_code_checksum[heli_codes_checksum_index++]=checksum;  

    //Serial.println(checksum);

    for (uint8_t i = 0; i < 34; i++) 
    {
      Serial.print(heli_codes[i]);
    }
    Serial.println("");
  }
}

void uniqueTHHeliLineDecoded()
{
  unsigned int checksum=0;
  unsigned int weight=1;
  bool found=false;
  char v=0;
  uint8_t i,ndx;

  //detectFreeRam();
  //Serial.println(freeRam);

  for (i = 0; i < 34; i++) 
  {
    v=heli_codes[i]-'0';
    if (v>1)
    {
      v=0;
    }

    checksum+=v*weight;
    weight+=117;
  }

  for (i = 0; i < heli_codes_checksum_index; i++)
  {
    if (heli_code_checksum[i]==checksum)
    {
      found=true;
      break;
    }
  } 

  if (!found)
  {
    heli_code_checksum[heli_codes_checksum_index++]=checksum;  

    for (i = 0; i < 34; i++) 
    {
      Serial.print(heli_codes[i]);
    }

    i=2;

    do
    {
      switch (i)
      {
      case 2:
        Serial.print(",Throttle,");
        break;

      case 10:
        Serial.print(",Rudder,");
        break;

      case 14:
        Serial.print(",Pitch,");
        break;      

      case 18:
        Serial.print(",Channel,");      
        break;      

      case 20:
        Serial.print(",Trim,");      
        break;      

      case 26:
        Serial.print(",AB,");
        break;      

      case 28:
        Serial.print(",Check,");      
        break;      
      }

      Serial.print(heli_codes[i++]);
    } 
    while (i<34);

    Serial.print(",MyCheck,");  
    int th = (heli_codes[ 4]-'0')*32+(heli_codes[ 5]-'0')*16+(heli_codes[ 6]-'0')*8+(heli_codes[ 7]-'0')*4+(heli_codes[ 8]-'0')*2+(heli_codes[ 9]-'0');
    int pi = (heli_codes[14]-'0')*8 +(heli_codes[15]-'0')*4 +(heli_codes[16]-'0')*2+(heli_codes[17]-'0');
    int tr = (heli_codes[20]-'0')*32+(heli_codes[21]-'0')*16+(heli_codes[22]-'0')*8+(heli_codes[23]-'0')*4+(heli_codes[24]-'0')*2+(heli_codes[25]-'0');
    int rd = (heli_codes[12]-'0')*32+(heli_codes[13]-'0')*16;

    int cl = (th + pi + tr + rd) & 63;
    int cla = cl;

    i=64;
    ndx=0;

    do
    {
      i /=2;
      if (cl>=i)
      {
        checksum1[ndx++]='1';
        cl -= i;
      }
      else
      {
        checksum1[ndx++]='0';
      }
    } 
    while (i!=1);

    bool m=true;

    for (i=0;i<6;i++)
    {
      Serial.print(checksum1[i]);

      if (checksum1[i]!=heli_codes[28+i])
      {
        m=false;
      }
    }

    if (m)
    {
      Serial.print(",OK,");      
    }
    else
    {
      Serial.print(",NG,");            
    }

    Serial.print(" ");
    Serial.print(th);
    Serial.print(" ");
    Serial.print(rd);
    Serial.print(" ");
    Serial.print(pi);
    Serial.print(" ");
    Serial.print(tr);

    Serial.print(" ");
    Serial.print(cla);
    Serial.print(" ");

    Serial.println("");
  }
}

void print2CHHeliLine(void) 
{
  bool enable = false;
  heli_codes_index=0;

  for (uint8_t i = 0; i < currentpulse-1; i++) 
  {
    bool error=true;
    int v = pulses[i][1];

    if ((heli_codes_index==14) && enable)
    {
      enable=false;
    }

    if (enable)
    {
      if ((v>90) && (v<99))
      {
        //Serial.print("0");
        heli_codes[heli_codes_index++]='0';
        error=false; 
      }

      if ((v>250) && (v<270))
      {
        //Serial.print("1");        
        heli_codes[heli_codes_index++]='1';
        error=false;         
      }        
    }

    if (v>350)
    {
      if (!enable)
      {
        //Serial.print("H");
        heli_codes_index=1;
        enable = true;
        error=false;       
        heli_codes[0]='H';
      }
      else
      {
        if (heli_codes_index==13)  // detect missed pulses between header pulses, do not print short frame...
        {
          //BUG: this outputs bits & decodes them
          unique2CHHeliLineDecoded();
        }
        heli_codes_index=0;
        enable = false;
      }
    }

    if (enable && error)  // if we are detecting, always include something, 1, 0 or E, header starts/stops detection
    {
      //Serial.print("E");
      heli_codes[heli_codes_index++]='E';
      //PATCH: stop capture, comment line below to print line with E
      enable = false;
    }
  }
}

void unique2CHHeliLineDecoded()
{
  unsigned int checksum=0;
  unsigned int weight=1;
  bool found=false;
  char v=0;
  uint8_t i,ndx;

  //detectFreeRam();
  //Serial.println(freeRam);

  for (i = 0; i < 13; i++) 
  {
    v=heli_codes[i]-'0';
    if (v>1)
    {
      v=0;
    }

    checksum+=v*weight;
    weight+=117;
  }

  for (i = 0; i < heli_codes_checksum_index; i++)
  {
    if (heli_code_checksum[i]==checksum)
    {
      found=true;
      break;
    }
  } 

  if (!found)
  {
    heli_code_checksum[heli_codes_checksum_index++]=checksum;  

    for (i = 0; i < 13; i++) 
    {
      Serial.print(heli_codes[i]);
    }

    i=1;

    do
    {
      switch (i)
      {
      case 1:
        Serial.print(",Channel,");
        break;

      case 3:
        Serial.print(",Throttle,");
        break;

      case 8:
        Serial.print(",Rudder,");
        break;      

      case 11:
        Serial.print(",Trim,");      
        break;      
      }

      Serial.print(heli_codes[i++]);
    } 
    while (i<13);

    Serial.println("");
  }
}

void detectFreeRam()
{
  int f=freeMemory();
  if (f<freeRam)
  {
    freeRam=f;
  }
}
